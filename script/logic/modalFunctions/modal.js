import {Modal} from "../../class/Modal.js";

function closeModal() {
    const modal = document.getElementById('our_modal')
    modal.addEventListener('click', event => {
        if (event.target.dataset.close) {
            const close = new Modal()
            close.close()
        }
    })
}

export {closeModal};