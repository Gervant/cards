export  default class Input {
    constructor(name, id, type, className, placeholder) {
        this.name = name;
        this.id = id;
        this.type = type;
        this.className = className;
        this.placeholder = placeholder;
    }
    renderInput(){
        const input = document.createElement('input');
        input.setAttribute('name', this.name);
        input.setAttribute('id', this.id);
        input.setAttribute('type', this.type);
        input.setAttribute('placeholder', this.placeholder);
        input.classList.add(this.className);
        return input;
    }
}