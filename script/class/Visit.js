import Input from "./Input.js";
import Select from "./Select.js";
import Textarea from "./Textarea.js";

export default class Visit {
    renderVisit() {
        const bodyModal = document.createElement('fieldset')
        bodyModal.classList.add('modal_body')
        this.named = new Input(
            'name',
            'input_name_modal',
            'text',
            'select_modal',
            "Введіть ваше ім'я",
            )
        this.doctor = new Select(
            ['Choose doctor', 'Cardiologist', 'Dentist', 'Therapist'],
            'doctor',
            'select_doctor',
            () => {},
            'select_modal',
            ''
        )

        this.urgency = new Select(
            ['Urgency', 'Law', 'Normal', 'High'],
            'urgency',
            'select_urgency',
            () => {},
            'select_modal',
            ''
        )
        this.status = new Select(
            ['Status', 'Open', 'Done'],
            'statusCard',
            'select_status',
            () => {},
            'select_modal',
            ''
        )
        this.purpose = new Input(
            'aimVisit',
            'purpose',
            'text',
            'select_modal',
            'Мета візиту',
        )
        this.description = new Textarea(
            'description',
            'desc_modal',
            'select_modal',
            'Короткий опис',
        )
        bodyModal.append(
            this.named.renderInput(),
            this.doctor.renderSelect(),
            this.urgency.renderSelect(),
            this.status.renderSelect(),
            this.purpose.renderInput(),
            this.description.renderTextarea()
        )
        this.doctor.hideFirstOption();
        this.urgency.hideFirstOption();
        return bodyModal
    }
}