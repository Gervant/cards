import Button from "./Button.js";
import Input from "./Input.js";

export default class VisitTherap {
    constructor() {

    }

    renderFormTherap() {
        const fieldset = document.createElement('fieldset')
        fieldset.classList.add('fieldset_doctor')
        fieldset.id = 'fieldset_doctor'
        const age = new Input('age', 'date_input', 'text', 'select_modal', 'Введіть ваш вік', '').renderInput()
        const button = new Button('btn_c', 'create_new_card', 'Створити картку', 'submit').createButton()
        fieldset.append(age, button)
        return fieldset
    }
}