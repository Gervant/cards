export default class Textarea {
    constructor(name, id, className, placeholder, ) {
        this.name = name;
        this.id = id;
        this.className = className;
        this.placeholder = placeholder;
    }

    renderTextarea(){
        const textArea = document.createElement('textarea');
        textArea.name = this.name;
        textArea.id = this.id;
        textArea.classList.add = this.className;
        textArea.setAttribute('placeholder', this.placeholder);
        return textArea;
    }
}