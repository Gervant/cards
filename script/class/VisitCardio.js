import Button from "./Button.js";
import Input from "./Input.js";

export default class VisitCardio{
    constructor() {

    }

    renderFormCardio(){
        const fieldset = document.createElement('fieldset')
        fieldset.classList.add('fieldset_doctor')
        fieldset.id = 'fieldset_doctor'
        const normalTreasure = new Input().renderInput();
        const bodyIndex = new Input().renderInput();
        const illnesses = new Input().renderInput();
        const age = new Input().renderInput();
        const button = new Button('btn_c', 'create_new_card', 'Створити кратку', 'submit').createButton();
        fieldset.append(normalTreasure, bodyIndex, illnesses, age, button);
        return fieldset
    }
}