import Button from "./Button.js";
import Input from "./Input.js";

export default class VisitDentist {
    constructor() {

    }

    renderFormDentist(){
        const fieldset = document.createElement('fieldset')
        fieldset.classList.add('fieldset_doctor')
        fieldset.id = 'fieldset_doctor'
        const date = new Input('date', 'date_input', 'text', 'select_modal', 'Дата останнього візиту', '').renderInput();
        const button = new Button('btn_c', 'create_new_card', 'Створити кратку', 'submit').createButton();
        fieldset.append(date, button)
        return fieldset
    }
}